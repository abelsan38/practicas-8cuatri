document.addEventListener('DOMContentLoaded', () => {
    appjs();
})
let res = document.getElementById("datos");
function appjs() {

    const formulario = document.getElementById('formulario');

    let valor;
    formulario.addEventListener('submit', (e) => {
        e.preventDefault();
        const input = document.getElementById('form').value;
        if (input.trim() === '') {
            const alerta = document.querySelector('.alerta');
            if (!alerta) {
                const h1 = document.createElement('h1')
                h1.innerHTML = 'Ingresa al menos un dato';
                h1.classList.add('alerta');
                res.appendChild(h1);
            }

            return;
        }
        apiFunction(input)

    })

}

function apiFunction(valor) {
    const http = new XMLHttpRequest;
    const url = `https://jsonplaceholder.typicode.com/albums/${valor}`;

    http.open('GET', url, true);
    http.send();
    //Validar la respusta 

    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {

            //Aqui se dibuja la pagina
            elimiar()

            const json = JSON.parse(this.responseText);

            const h1 = document.createElement('h1');

            h1.innerText = json.title;



            res.appendChild(h1);

            // CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTOS 


        } else {
            const alerte = document.querySelector('.alerta');
            if (!alerte) {
                const h1 = document.createElement('h1');
                h1.innerText = 'Error al consultar los datos';
                h1.classList.add('alerta');
                res.appendChild(h1);
            }

        }
    }

}
function elimiar() {
    while (res.firstChild) {
        res.removeChild(res.firstChild)

    }
}