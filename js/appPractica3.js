let res = document.getElementById('lista')
function appjs() {
    const http = new XMLHttpRequest;
    const url = 'https://jsonplaceholder.typicode.com/users';

    http.open('GET', url, true);//el true se utiliza para hacer que la app
    //async es decir que se corra el codigo pricipal junto con el otro 
    //y el false quiere decir que s sincrona que esto es una mala practica
    http.send();

    http.onreadystatechange = function () {
        eliminarInfoPrevia()
        if (this.status == 200 && this.readyState == 4) {

            const jsons = JSON.parse(this.responseText);

            jsons.forEach(json => {
                //crear elementos 
                const tr = document.createElement('tr');
                const tdid = document.createElement('td');
                const tdname = document.createElement('td');
                const tdUserName = document.createElement('td');
                const email = document.createElement('td');
                const street = document.createElement('td');
                const suite = document.createElement('td');
                const city = document.createElement('td');
                const zipcode = document.createElement('td');
                const lat = document.createElement('td');
                const lng = document.createElement('td');

                //agregar clases
                tdid.classList.add('columna1');
                tdname.classList.add('columna2');
                tdUserName.classList.add('columna3');
                email.classList.add('columna4');
                street.classList.add('columna5');
                suite.classList.add('columna6');
                city.classList.add('columna7');
                zipcode.classList.add('columna8');
                lat.classList.add('columna9');
                lng.classList.add('columna10');
                //insertar texto
                tdid.innerText = json.id;
                tdname.innerText = json.name;
                tdUserName.innerText = json.username;
                email.innerText = json.email;
                street.innerText = json.address.street;
                suite.innerText = json.address.suite;
                city.innerText = json.address.city;
                zipcode.innerText = json.address.zipcode;
                lat.innerText = json.address.geo.lat;
                lng.innerText = json.address.geo.lng;
                tr.appendChild(tdid)
                tr.appendChild(tdname)
                tr.appendChild(tdUserName)
                tr.appendChild(email)
                tr.appendChild(street)
                tr.appendChild(suite)
                tr.appendChild(city)
                tr.appendChild(zipcode)
                tr.appendChild(lat)
                tr.appendChild(lng)

                res.appendChild(tr)



            });
        } else {
            console.log('error');
        }
    }

}
document.getElementById('btnCargar').addEventListener('click', () => {
    appjs()
})
document.getElementById('btnLimpiar').addEventListener('click', () => {
    eliminarInfoPrevia()
})

function eliminarInfoPrevia() {
    while (res.firstChild) {
        res.removeChild(res.firstChild)
    }
}
