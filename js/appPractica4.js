document.addEventListener('DOMContentLoaded', () => {
    appFetch();
})
let res = document.getElementById('datos');
function appFetch() {
    const formulario = document.getElementById('formulario');

    formulario.addEventListener('submit', (e) => {
        e.preventDefault();
        let valor = document.getElementById('value').value;
        if (valor.trim() === '') {
            eliminar()
            const text = document.querySelector('.text-white')
            if (!text) {
                const h1 = document.createElement('h1');
                res.style.backgroundColor = '#cb3234';
                res.style.fontWeight = 'bold';
                h1.classList.add('text-center', 'text-white', 'p-1')
                h1.innerText = 'input vacio'
                res.appendChild(h1)

            }
            return;
        }
        apisById(valor);

    })
}
function apisById(value) {
    const http = new XMLHttpRequest;
    const url = `https://jsonplaceholder.typicode.com/users/${value}`;

    http.open('GET', url, true);
    http.send()

    http.onreadystatechange = function () {
        eliminar();
        if (this.status == 200 && this.readyState == 4) {
            const jsons = JSON.parse(this.responseText);

            res.classList.add('p-3')
            res.style.backgroundColor = '#f1f6db';
            const tr = document.createElement('div');
            const div = document.createElement('div');
            const div2 = document.createElement('div');
            const div3 = document.createElement('div');
            const h2 = document.createElement('h2');
            const h2cordenadas = document.createElement('h2');
            const tdid = document.createElement('h1');
            const tdname = document.createElement('h1');
            const tdUserName = document.createElement('p');
            const email = document.createElement('p');
            const street = document.createElement('p');
            const suite = document.createElement('p');
            const city = document.createElement('p');
            const zipcode = document.createElement('p');
            const lat = document.createElement('p');
            const lng = document.createElement('p');

            tdid.innerText = ` #${jsons.id}`;
            tdname.innerText = jsons.name;
            tdUserName.innerText = `user: ${jsons.username}`;
            email.innerText = jsons.email;
            street.innerText = jsons.address.street;
            suite.innerText = jsons.address.suite;
            city.innerText = jsons.address.city;
            zipcode.innerText = jsons.address.zipcode;
            lat.innerText = jsons.address.geo.lat;
            lng.innerText = jsons.address.geo.lng;
            h2.innerText = 'Direccion'
            h2cordenadas.innerText = 'Coordenadas'

            div.classList.add('div')
            div.appendChild(tdname)
            div.appendChild(tdid)
            div2.classList.add('div')
            div2.appendChild(tdUserName)
            div2.appendChild(email)
            div3.classList.add('grid');
            div3.appendChild(street)
            div3.appendChild(suite)
            div3.appendChild(city)
            div3.appendChild(zipcode)
            tr.appendChild(div)
            tr.appendChild(div2)
            tr.appendChild(h2)
            tr.appendChild(div3)
            tr.appendChild(h2cordenadas)

            tr.appendChild(lat)
            tr.appendChild(lng)

            res.appendChild(tr)

        } else {
            const text = document.querySelector('.text-white')
            if (!text) {
                const h1111 = document.createElement('h1');
                res.style.backgroundColor = '#cb3234';
                res.style.fontWeight = 'bold';
                h1111.classList.add('text-center', 'text-white')
                h1111.innerText = 'Error al cargar los datos el ID no existe o intente de nuevo'
                res.appendChild(h1111)
                return;
            }

        }
    }
}
function eliminar() {
    while (res.firstChild) {
        res.removeChild(res.firstChild)
    }
}