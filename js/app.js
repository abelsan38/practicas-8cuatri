function hacerPeticion() {
    const http = new XMLHttpRequest;
    const url = 'https://jsonplaceholder.typicode.com/albums';

    http.open('GET', url, true);
    http.send();
    //Validar la respusta 

    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {

            //Aqui se dibuja la pagina

            let res = document.getElementById("lista");
            const json = JSON.parse(this.responseText);

            // CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTOS 

            for (const datos of json) {
                res.innerHTML += '<tr> <td class = "columna1">' + datos.userID + '</td>'
                    + '<td class = "columna2">' + datos.id + '</td>'
                    + '<td class = "columna3">' + datos.title + '</td> </tr>'
            }
            res.innerHTML += "</tbody>"
        }
    }

}

// Codificar los botones

document.getElementById('btnCargar').addEventListener('click', () => {
    hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", () => {
    let res = document.getElementById("lista");
    res.innerHTML = "";
})